
## About Glasster
Glasster is a framework based on laravel for create ACL (Access Control Level) for manage Authentication system , Roles and permissions , with Blog System.

- Authentication system Login , Register , ForgotPassword
- OTP login and register system with Email , SMS and Google authenticator
- Blog manage system with advance SEO configs
- Email Template manage system 
- General SEO configuration <br>
and etc ...
## Use Grasster

for using Glasster yo most be download glasster with this command
```
composer create-project luxiloom/glasster
```

after download you most be configuration DataBase from `.env` file after you can run `setup` command for setup all requirment and configure glasster.
```
php artisan setup
```

## Author

Glasster create by [Hadi Mo](https://codeindev.com) . you can contact me on `hi@codeindev.com` Email.
<br>

## License

Glasster freamework based on Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
